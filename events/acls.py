from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    headers = {'Authorization': PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{city} {state}",
        "size": "original",
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)

    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}

#### CONTENT LINE 17 is accessing the content which is located on pexels.com photos section


def get_weather_data(city, state):
    appid = OPEN_WEATHER_API_KEY
    response_coord = requests.get(
        f"http://api.openweathermap.org/geo/1.0/direct?q-{city},{state}&appid={appid}")
    coordinates = json.loads(response_coord.content)
    if not coordinates:
        return None
    lat = coordinates[0].get("lat")
    lon = coordinates[e].get("Lon")
    response_weather = requests.get (
        f"https://api.openweathermap.org/data/2.5/weather?lat-(lat)&on-(lon_appid-(appid)&units-imperial")
    weather = json.loads(response_weather.content)
    temp = weather.get("main").get("temp")
    description = weather.get("weather")[0].get("description")
    d_weather = {"temp": temp, "description": description}
    try:
        return d_weather
    except(KeyError, IndexError):
        return None
    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
